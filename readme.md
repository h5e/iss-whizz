# Iss Whizz

Welcome to Iss Whizz, the important public service Mastodon bot which lets you know whenever someone on the ISS takes a whizz. At least when using the toilet in the [Tranquility module](https://en.wikipedia.org/wiki/Tranquility_(ISS_module)). From what I gather they also dump the output created in the Russian module and EVAs in there, causing large spikes, but I don't really _know_.

The bot is live at https://mas.to/@iss_whizz, make sure to follow so you'll never miss another opportunity to impress your friends with your intimate knowledge of the goings-on on the ISS. If this is not enough for you and you're on a Mac, check out [pISSStream](https://github.com/Jaennaet/pISSStream) to make your life complete.

It was made by [Hanneke](https://hanneke.rocks). She's really proud of this.

## Running the thing

I'm assuming you know the basics here, so I'm going to gloss over a few things.

First create a bot on Mastodon, you only need access to write statuses. Make a note of the instance url and your access token.

### Node

Set the environment variables, or use a `.env` file in the `node` directory. Run `npm install` and `npm run start` and off you go.

### Docker

Either pull the container from this repo's registry or build it using the `Dockerfile` and run that one. Create a `.env` file or use the `--env` option.

Example:

```shell
    docker run --env-file .env registry.gitlab.com/h5e/iss-whizz:latest
```

## FAQ

Some answers to questions I imagine people would ask frequently if I hadn't answered them here.

### Hanneke why? _Why?_
Peepee poopoo funny.

### So why no poopoo then?
I don't know what values to watch, if you do please [tell me](https://tech.lgbt/@h5e)! I would love to inform you whenever someone drops anchor on the ISS. Although dropping is probably the wrong word here.

### How does it work?
The telemetry data from the ISS contains, among many, many other things I don't know what to do with, the fill level of the urine tank. Whenever this fill level increases this bot will toot out an update.

### Where do you get the data?
The data is provided by [LightStreamer](https://lightstreamer.com/), see their [example](https://github.com/Lightstreamer/Lightstreamer-example-ISSLive-client-javascript) or [blog post](https://blog.lightstreamer.com/2014/02/how-nasa-is-using-lightstreamer.html) for more information. Another cool project using this data is [Mimic](https://iss-mimic.github.io/Mimic/).

### How large is that tank anyway? Percentages are nice and all, but _how much_?
Look, if I knew I would have told you.

### Do you consider this code… good?
Let's stick with "good enough".

### What about PRs?
I don't think I have the spoons for that but if you really want to go ahead.
