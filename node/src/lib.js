export const pick = (array) => {
    return array[Math.trunc(Math.random() * array.length)];
}

export const debounce = (fn, wait) => {
    wait = Math.max(0, wait);

    let timeout = undefined;

    return (...args) => {
        if (timeout !== undefined) {
            clearTimeout(timeout);
        }
        timeout = setTimeout(() => fn.apply(undefined, args), wait);
    }
}

export const retry = async (fn, retries, wait) => {
    try {
        return await fn();
    } catch (e) {
        console.warn(`Executing function failed: ${e}`);
        if (retries > 0) {
            console.log(`Retry in ${wait} ms`);
            await delay(wait);
            return await retry(fn, retries - 1, wait * 2);
        } else {
            console.log(`Out of retries; rethrowing`);
            throw e;
        }
    }
}

export const delay = (ms) => {
    return new Promise((res) => setTimeout(res, ms));
}
