import * as env from "dotenv";
import {createRestAPIClient} from "masto";
import {LightstreamerClient, Subscription} from "lightstreamer-client-node";
import {debounce, pick, retry} from "./lib.js";

env.config();

console.log(`Starting bot at: ${process.env.MASTODON_URL}`);

const itemProcessorState = "NODE3000004"; // might be interesting?
const itemProcessorFill = "NODE3000005";

const fieldTimestamp = "TimeStamp";
const fieldValue = "Value";

const processorStates = {
    2: "STOP",
    4: "SHUTDOWN",
    8: "MAINTENANCE",
    16: "NORMAL",
    32: "STANDBY",
    64: "IDLE",
    128: "SYSTEM INITIALIZED"
};

const getProcessorState = (state) => {
    if (state in processorStates) {
        return processorStates[state];
    } else {
        return `UNKNOWN (${state})`;
    }
}

const stupidRemarksAboutAWhizz = [
    "I'm sure they feel better now!",
    "What a relief.",
    "Hope they didn't spring a leak.",
    "Well done!",
    "I'm impressed.",
    "I just tell myself it's raining."
];

let lastPercentage = undefined;

const mastodonClient = createRestAPIClient({
    url: process.env.MASTODON_URL,
    accessToken: process.env.MASTODON_ACCESS_TOKEN,
});

const lightstreamerClient = new LightstreamerClient("https://push.lightstreamer.com/","ISSLIVE");
lightstreamerClient.connect();
lightstreamerClient.addListener({
    onStatusChange(chngStatus) {
        console.log(`Lightstream status: ${chngStatus}`);
    },
    onServerError(errorCode, errorMessage) {
        console.error(`Server error; code: ${errorCode}, message: ${errorMessage}`);
        process.exit(1);
    },
    onListenEnd() {
        console.error("Lightstream listen end");
        process.exit(1);
    }
});

const issSubscription = new Subscription("MERGE",[itemProcessorState, itemProcessorFill],[fieldTimestamp, fieldValue]);
issSubscription.setRequestedSnapshot("yes");
lightstreamerClient.subscribe(issSubscription);

issSubscription.addListener({
    onUnsubscription() {
        console.error("Subscription unsubscribed");
        process.exit(1);
    },
    onSubscriptionError(errorCode, errorMessage) {
        console.error(`Subscription error; code: ${errorCode}, message: ${errorMessage}`);
        process.exit(1);
    },
    onItemUpdate(obj) {
        const timeStamp = parseFloat(obj.getValue(fieldTimestamp));

        switch (obj.getItemName()) {
            case itemProcessorState:
                console.log(`Processor state update; timeStamp: ${timeStamp}, state: ${getProcessorState(obj.getValue(fieldValue))}`);
                break;
            case itemProcessorFill:
                const percentage = parseFloat(obj.getValue(fieldValue));

                if (obj.isSnapshot()) {
                    console.log(`Tank level snapshot; timeStamp: ${timeStamp}, percentage: ${percentage}`);
                    lastPercentage = percentage;
                    break;
                }

                console.log(`Tank level update; timeStamp: ${timeStamp}, percentage: ${percentage}`);
                onTankUpdateDebounced(timeStamp, percentage);
                break;
        }
    }
});

const onTankUpdate = async (timeStamp, newPercentage) => {
    if (lastPercentage === undefined) {
        console.log(`Last tank level unknown; setting to ${newPercentage}`);
        lastPercentage = newPercentage;
        return;
    }

    const change = newPercentage - lastPercentage;
    if (change === 0) {
        return;
    }

    if (change > 0) {
        console.log(`Tank filled by ${change}% to a total of ${newPercentage}`);
        await tootAboutAWhizz(lastPercentage, change, newPercentage);
        lastPercentage = newPercentage;
        return;
    }

    // The tank level can fluctuate a bit
    // Fixme: emptying goes slowly in which case we shouldn't ignore this
    if (change >= -2) {
        console.log(`Tank level lowered by ${change}; ignoring`);
        // purposefully not setting lastPercentage
        return;
    }

    if (newPercentage <= 10) {
        console.log(`Tank emptied by ${change} to ${newPercentage}; maybe we should tell the world where they emptied it?`);
    }

    lastPercentage = newPercentage;
}

const tootAboutAWhizz = async (oldPercentage, change, newPercentage) => {
    const message = `Someone on the ISS took a whizz! They increased the tank fill level by ${change} percentage point${change !== 1 ? 's' : ''} to ${newPercentage}%. ${pick(stupidRemarksAboutAWhizz)}`;
    await sendToot(message)
}

const sendToot = async (statusMessage) => {
    console.log('Sending toot:', statusMessage);
    retry(async () => {
        const status = await mastodonClient.v1.statuses.create({
            status: statusMessage,
            visibility: "unlisted", //"public" | "unlisted" | "private" | "direct";
        });
        console.log('Toot at', status.url);
    }, 3, 1000);
}

const onTankUpdateDebounced = debounce(onTankUpdate, 15000);
